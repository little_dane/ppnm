My student number is 201707886 making the last two digits X = 86. The number of projects is N = 22, thus my exam number is 86 mod 22 = 20.

-- #20 --
Adaptive integration of complex-valued functions
Implement an adaptive integator which calculates the integral of a complex-valued function f(z) of a complex variable z along a straight line between two points in the complex plane.
