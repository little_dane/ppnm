from integrator import * 
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as sp

	#Defining functions	
def errorf(z):
	f = lambda t: np.exp(-t**2,dtype = complex)
	return 2/np.sqrt(np.pi)*integrator(f,0,z)[0]
def gammaf(z):
	f = lambda t: np.power(np.log(1/t),z-1)
	return integrator(f,0.,1.)[0]



def exam():
	print('\nProblem 20 - Adaptive integration of complex-valued functions\n')
	print('\nSome simple complex functions are tested.\n')
	
	f = lambda z: z*z+3j*z
	a = 1+2j
	b = 4-1j 
	print('\nFirst function tested is z^2+3*i*z from 1+2i to 4-1i')
	Q,err,steps = integrator(f,a,b)
	print('Analytical result is 39 + 12i')
	print('Calculated result is {} with error {}'.format(Q,err))

	g = lambda z: np.sin(3*z)*1j
	a = -np.pi+3j
	b = 2*np.pi+5j
	print('\nNext function tested is i*sin(3*z) from -pi+3i to 2pi+5i')
	Q,err,steps = integrator(g,a,b,1e-5,1e-5)
	print('Analytical result is -1/3i(cos(9i)+cos(15i)) = 0+546186.743...i')
	print('Calculated result is {} with error {}'.format(Q,err))

	print('\nNow, 1/z is integrated on the closed loop around the unit sphere')
	print('This is done by parameterising the function 1/z to exp(it), where t is between 0 and 2pi')
	h = lambda z: 1j*np.exp(1j*z)/np.exp(1j*z)
	a = 0 
	b = 2*np.pi
	Q,err,steps = integrator(h,a,b)
	print('Analytical result is 2pi*i')
	print('Calculated result is {} with error {}'.format(Q,err))


	print('\nError function')
	xs = np.linspace(-3,3,100,dtype = complex)
	zs = np.linspace(-3j,3j,100,dtype = complex)
	lim = [-2j, -1j, 0, 1j, 2j]
	
	print('Figure Errorfunction.png shows the  plot of the error function in both the real and the imaginary axes with different z-values.')

		#Plot
	fig,ax = plt.subplots(2,2)		
	for l in lim:
		erfs = [errorf(l+x) for x in xs]
		ax[0,0].plot(xs,erfs,label=str(int(np.imag(l))))
		ax[0,1].plot(xs,np.imag(erfs))
		erfs = [errorf(l*(-1j)+z) for z in zs]
		ax[1,0].plot(zs*(-1j),erfs,label=str(int(np.imag(l))))
		ax[1,1].plot(zs*(-1j),np.imag(erfs))

	ax[0,0].set_xlabel('real(z)')
	ax[1,0].set_xlabel('imag(z)')
	ax[0,1].set_xlabel('real(z)')
	ax[1,1].set_xlabel('imag(z)')

	ax[1,0].set_ylabel('real(erf(z))')
	ax[0,0].set_ylabel('real(erf(z))')
	ax[0,1].set_ylabel('imag(erf(z))')
	ax[1,1].set_ylabel('imag(erf(z))')
	
	ax[0,0].legend(title='imag(z)',bbox_to_anchor=(1.04,0.5),loc="center left")
	ax[1,0].legend(title='real(z)',bbox_to_anchor=(1.04,0.5),loc="center left")
	
	ax[0,0].grid(True)
	ax[1,0].grid(True)
	ax[0,1].grid(True)
	ax[1,1].grid(True)

	fig.tight_layout()
	fig.savefig("Errorfunction")
	plt.close()
	print('Value of erf(1+0.5i) is 0.9507+0.1880i')
	print('Calculated value of erf(1+0.5i) is {}'.format(errorf(1+0.5j)))
	print('Value of erf(4) is 0.9999')
	print('Calculated value of erf(4) is {}'.format(errorf(4)))



	print('\nGamma function')
	print('Figure Gammafunction.png shows the  plot of the gamma function in both the real and the imaginary axes with different z-values.')
	xs = np.linspace(1,5,100)
	zs = np.linspace(1j,5j,100,dtype = complex)
	zlim = [1j,2j,3j,4j,5j]
	xlim = [1,2,3,4,5]

		#Plot
	fig,ax = plt.subplots(2,2)
	for lz,lx in zip(zlim,xlim):
		gams = [gammaf(x+lz) for x in xs]
		ax[0,0].plot(xs,gams,label=str(int(np.imag(lz))))
		ax[0,1].plot(xs,np.imag(gams))
		gams = [gammaf(z+lx) for z in zs]
		ax[1,0].plot(zs*(-1j),gams,label=str(lx))
		ax[1,1].plot(zs*(-1j),np.imag(gams))
	
	ax[0,0].set_xlabel('real(z)')
	ax[1,0].set_xlabel('imag(z)')
	ax[0,1].set_xlabel('real(z)')
	ax[1,1].set_xlabel('imag(z)')

	
	ax[1,0].set_ylabel(r'real($\Gamma$(z))')
	ax[0,0].set_ylabel(r'real($\Gamma$(z))')
	ax[1,1].set_ylabel(r'imag($\Gamma$(z))')
	ax[0,1].set_ylabel(r'imag($\Gamma$(z))')	
	
	ax[0,0].legend(title='imag(z)',bbox_to_anchor=(1.04,0.5),loc="center left")
	ax[1,0].legend(title='real(z)',bbox_to_anchor=(1.04,0.5),loc="center left")
	
	ax[0,0].grid(True)
	ax[1,0].grid(True)
	ax[0,1].grid(True)
	ax[1,1].grid(True)

	
	fig.tight_layout()
	fig.savefig('Gammafunction')
	plt.close()
	print('Value of gamma(1+0.5i) is 0.8017-0.1996i')
	print('Calculated value of gamma(1+0.5i) is {}'.format(gammaf(1+0.5j)))
	print('Value of gamma(4) is 6')
	print('Calculated value of gamma(4) is {}'.format(gammaf(4)))


	
