import numpy as np
import sys
import math
sys.setrecursionlimit(100000)


#The recursive integrator
def rec_int(f, a, b, acc, eps, f2, f3, steps=0):
	f1 = f(a + (b-a)*1/6)
	f4 = f(a + (b-a)*5/6)

	Q = (2*f1 + f2 + f3 + 2*f4)/6 * (b-a)
	q = (f1 + f4 + f2 + f3)/4 * (b-a)
	
	tol = acc + eps*abs(Q)
	err = abs(Q-q)
	
	if err < tol:
		return Q,err,steps
	else:
		steps += 1
		Q1,err1,steps1 = rec_int(f, a, (a+b)/2, acc/np.sqrt(2.), eps, f1, f2,steps)
		Q2,err2,steps2 = rec_int(f, (a+b)/2, b, acc/np.sqrt(2.), eps, f3, f4,steps)
		Q = Q1+Q2
		err = np.sqrt(err1**2+err2**2)
		steps = max(steps1,steps2)
		return Q,err,steps

#The main integrator
def integrator(f, a, b, acc=1e-3, eps=1e-3):
	#Create f_applied to potentially change f if infinite limits
	f_applied = f
	#Check for infinite limits
	if np.isinf(a):	
		if np.isinf(b):	#Both limits infinite
			f_applied = lambda t: f(t/(1-t**2))*(1+t**2)/((1-t**2)**2)
			a = -1
			b = 1
		else:			#Only lower limit infinite
			f_applied = lambda t: f(b-(1-t)/t)/(t**2)
			a = 0
			b = 1
	elif np.isinf(b):		#Only upper limit infinite
		f_applied = lambda t: f(a+(1-t)/t)/(t**2)	
		a = 0.
		b = 1.

	f2 = f_applied(a + 2*(b-a)*1/6)
	f3 = f_applied(a + 4*(b-a)*1/6)
	return rec_int(f, a, b, acc, eps, f2, f3)

#The Clenshaw-Curtis transformation
def clenshawCurtis(f, a, b, acc=1e-3, eps=1e-3):
	g = lambda theta: f((b-a)/2*np.cos(theta)+(a+b)/2)*np.sin(theta)*(b-a)/2	#Rescaling using Gauss-Legendre
	return integrator(g,0,np.pi,acc,eps)
