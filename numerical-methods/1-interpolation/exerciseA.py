import numpy as np
import matplotlib.pyplot as plt
from math import *
import random



# Functions #
"""
The function binsearch searches x for z. If there is no exact value, it returns the integer of the closest lower value.
"""
def binsearch(x,z):
	i = 0
	j = len(x)-1
	mid = floor((i+j)/2.)
	while(i <= j):
		if x[mid] == z:
			break
		elif x[mid] < z:
			i = mid+1
		elif x[mid] > z:
			j = mid-1
		mid = floor((i+j)/2.)
	return mid
"""
The function linterp should make a linear interpolation at a given point z.
"""
def linterp(x:list, y:list, z:float):
	mid = binsearch(x,z) 
	p = (y[mid+1]-y[mid])/(x[mid+1]-x[mid])
	return y[mid]+p*(z-x[mid]),mid

"""
The function linterpInteg should do numerical linear integration from x[0] to the interpolated point z
"""
def linterpInteg(x:list, y:list, z:float):
	yz,mid = linterp(x,y,z)
	x = x[:mid+1] + [z]
	y = y[:mid+1] + [yz]
	s = 0
	for i in range(len(x)-1):
		a = (y[i+1]-y[i])/(x[i+1]-x[i])
		b = y[i] - a*x[i]
		s += a/2.*(x[i+1]**2 - x[i]**2) + b*(x[i+1] - x[i])
	return s


# Testing #

x_test = list(range(1,11))
y_test = [1, 2, 4, 8, 16, 32, 40, 44, 46, 47]
z_test = [min(x_test)+random.random()*(max(x_test)-min(x_test)) for i in range(50)]
z_test.sort()
yz = []
At = []
for i in range(len(z_test)):
		yl,_ = linterp(x_test,y_test,z_test[i])
		yz += [yl]
		Al = linterpInteg(x_test,y_test,z_test[i])
		At += [Al]

# Plot #

figure,ax = plt.subplots(2,1,figsize=(18,8))

ax[0].plot(z_test,yz,'r.',label='Splined points')
ax[0].plot(x_test,y_test,'b*',label='Data')

ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].grid(True)
ax[0].set_axisbelow(True)
ax[0].legend()
ax[0].set_title('Linear Spline')

ax[1].plot(z_test,At,'r.')
ax[1].set_xlabel('z')
ax[1].set_ylabel('Integrated value')
ax[1].grid(True)
ax[1].set_axisbelow(True)

figure.tight_layout()
figure.savefig('figureA')

print('\nProblem A - Linear spline')
print('See linear spline in figureA.png')
print('\n\n')
