import numpy as np
import matplotlib.pyplot as plt
from math import *
import random

# Functions #
"The binseach function is identical to the one in exercise A"
def binsearch(x,z):
	i = 0
	j = len(x)-1
	mid = floor((i+j)/2.)
	while(i <= j):
		if x[mid] == z:
			break
		elif x[mid] < z:
			i = mid+1
		elif x[mid] > z:
			j = mid-1
		mid = floor((i+j)/2.)
	return mid

"This is the analytical solution to the integral used to avoid complicated expressions"
def intex(y, b, c, xi, z):
	return y*z+b*(z**2/2.-xi*z)+c*(z**3/3.-xi**2*z)

"""
The qspline function is the function that generates the quadratic spline.
"""
def qspline(x:list, y:list, z:float, res:int):
	p = [(y[i+1] - y[i]) / (x[i+1] - x[i]) for i in range(len(x)-1)]
	c = [0]
	for i in range(len(x)-2):
		c += [(p[i+1] - p[i] - c[i] * (x[i+1] - x[i])) / (x[i+2] - x[i+1])]
	b = [p[i] - c[i] * (x[i+1] - x[i]) for i in range(len(x)-1)]
	m = binsearch(x,z)
	yz = y[m] + b[m] * (z-x[m]) + c[m] * (z-x[m])**2

	if res == 1:
		return b[m]+2*c[m]*(z-x[m])
	elif res == -1:
		x = x[:m+1] + [z]
		y = y[:m+1] + [yz]
		s = 0
		for i in range(len(x)-1):
			s += intex(y[i], b[i], c[i], x[i], x[i+1])-intex(y[i], b[i], c[i], x[i], x[i])
		return s
	elif res == 0:
		return yz
	else:
		print('Invalid definition of res')
		return None

# Testing #

xt = list(range(1,11))
yt = [1, 2, 4, 8, 16, 32, 40, 44, 46, 47]
zt = [min(xt)+random.random() * (max(xt)-min(xt)) for i in range(50)]
zt.sort()

st = [qspline(xt, yt, zt[i], res = 0) for i in range(len(zt))]
dt = [qspline(xt, yt, zt[i], res = 1) for i in range(len(zt))]
at = [qspline(xt, yt, zt[i], res = -1) for i in range(len(zt))]

# Plot #

figure,ax = plt.subplots(3,1,figsize=(18,8))

ax[0].plot(zt, st, 'r.', label = 'Spline')
ax[0].plot(xt, yt, 'b*', label = 'Data')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].legend()
ax[0].grid(True)
ax[0].set_axisbelow(True)
ax[0].set_title('Quadratic spline')

ax[1].plot(zt, dt, 'r.')
ax[1].set_xlabel('z')
ax[1].set_ylabel('S\'(z)')
ax[1].grid(True)
ax[1].set_axisbelow(True)

ax[2].plot(zt, at, 'r.')
ax[2].set_xlabel('z')
ax[2].set_ylabel('Integrated value')
ax[2].grid(True)
ax[2].set_axisbelow(True)

figure.tight_layout()
figure.savefig('figureB')
print('\nProblem B - Quadratic spline')
print('See quadratic spline in figureB.png')
print('\n\n')

