import numpy as np
import matplotlib.pyplot as plt
from math import *
import random
from scipy import interpolate

# Functions #

"Functions intex and binsearch are implemented similarly to exercise B"
def binsearch(x,z):
	i = 0
	j = len(x)-1
	mid = floor((i+j)/2.)
	while (i <= j):
		if x[mid] == z:
			break
		elif x[mid] < z:
			i = mid+1
		elif x[mid] > z:
			j = mid-1
		mid = floor((i+j)/2.)
	return mid

def intex(y, h, b, c, d):
	return y*h+1/2*b*h**2+1/3*c*h**3+1/4*d*h**4

"Cubic spline defined below"
def cspline(x,y,z,res=0):
    n = len(x)
    h = [x[i+1]-x[i] for i in range(n-1)]
    p = [(y[i+1]-y[i])/h[i] for i in range(n-1)]
    
    D = [2]; Q = [1]; B = [3*p[0]];
    D += [2*h[i]/h[i+1]+2 for i in range(n-2)]
    D += [2]
    Q += [h[i]/h[i+1] for i in range(n-2)]
    B += [3*(p[i]+p[i+1]*h[i]/h[i+1]) for i in range(n-2)]
    B += [3*p[-1]]
    
    for i in range(1,n):
        D[i] -= Q[i-1]/D[i-1]
        B[i] -= B[i-1]/D[i-1]
    
    b = [0 for i in range(n)]
    b[n-1] = B[-1]/D[-1]
    
    for i in range(n-2,-1,-1):
        b[i]=(B[i]-Q[i]*b[i+1])/D[i];
    
    c = [(-2*b[i]-b[i+1]+3*p[i])/h[i] for i in range(n-1)]
    d = [(b[i]+b[i+1]-2*p[i])/h[i]/h[i] for i in range(n-1)]

    m = binsearch(x,z)
    yz = y[m] + b[m]*(z-x[m]) + c[m]*(z-x[m])**2 + d[m]*(z-x[m])**3

    if res==0:
        return yz
    elif res==-1:
        x = x[:m+1] + [z]
        y = y[:m+1] + [yz]
        h = h[:m] + [z-x[m]]
        s = 0
        for i in range(len(x)-1):
            s += intex(y[i],h[i],b[i],c[i],d[i])
        return s
    elif res==1:
        return b[m]+2*c[m]*(z-x[m])+3*d[m]*(z-x[m])**2
    else:
        print('Invalid definition of res')
        return None

# Testing #

xt = list(range(1,11))
yt = [1,2,4,8,16,32,40,44,46,47]
zt = [min(xt)+random.random()*(max(xt)-min(xt)) for i in range(50)]
zt.sort()

st = [cspline(xt,yt,zt[i],0) for i in range(len(zt))]
dt = [cspline(xt,yt,zt[i],1) for i in range(len(zt))]
it = [cspline(xt,yt,zt[i],-1) for i in range(len(zt))]

tck = interpolate.splrep(xt,yt,s=0)
ynew = interpolate.splev(zt,tck,der=0)

# Plot #

figure,ax = plt.subplots(4,1,figsize=(18,8))

ax[0].plot(zt, st, 'r.', label = 'Spline')
ax[0].plot(xt, yt, 'b*', label = 'Data')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].legend()
ax[0].grid(True)
ax[0].set_axisbelow(True)
ax[0].set_title('Cubic spline')

ax[1].plot(zt, dt, 'r.')
ax[1].set_xlabel('z')
ax[1].set_ylabel('S\'(z)')
ax[1].grid(True)
ax[1].set_axisbelow(True)

ax[2].plot(zt,it,'.r')
ax[2].set_xlabel('z')
ax[2].set_ylabel('Integrated value')
ax[2].grid()
ax[2].set_axisbelow(True)

ax[3].plot(zt,st-ynew,'.b')
ax[3].set_xlabel('z')
ax[3].set_ylabel('Deviation from library')
ax[3].grid()
ax[3].set_axisbelow(True)

figure.tight_layout()
figure.savefig('figureC')

print('\nProblem C - Cubic spline')
print('See cubic spline in figureC.png')
print('\n\n')
