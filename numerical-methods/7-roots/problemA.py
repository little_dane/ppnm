import numpy as np
from math import *
from linearequations import *
from root import *

	#Functions
def simple1D_grad(p):
	return 2*p[0]

def simple2D_gradx(p):
	return 2*p[0]
def simple2D_grady(p):
	return 12*p[1]**3

def rosenbrock_gradx(p):
	return -2+2*p[0]-400*p[0]*p[1]+400*p[0]**3
def rosenbrock_grady(p):
	return 200*(p[1]-p[0]**2)





def problemA():
	print('\nProblem A - Test my root finder\n')
		#1D simple function
	print('\nSimple 1D function: x^2')
	simple_grad = [simple1D_grad]
	x0 = [19]
	res,_ = newton(simple_grad,x0)
	print('Expected minimum: 0')
	print('Starting guesses: x = {}'.format(x0[0]))
	print('Found minimum: {}'.format(res[0]))
		#2D simple function
	print('\nSimple 2D function: x^2+3y^4+5')
	simple2D_grad = [simple2D_gradx,simple2D_grady]
	x0 = [1,37]
	res,_ = newton(simple2D_grad,x0)
	print('Expected minimum: (0,0)')
	print('Starting guesses: x = {}, y = {}'.format(x0[0],x0[1]))
	print('Found minimum: x = {}, y = {}'.format(res[0],res[1]))
		#Rosenbrock's valley function
	print("\nRosenbrock's valley function")
	rosenbrock_grad = [rosenbrock_gradx,rosenbrock_grady]
	x0 = [0,2]
	res,_ = newton(rosenbrock_grad,x0)
	print('Expected minimum: (1,1)')
	print('Starting guesses: x = {}, y = {}'.format(x0[0],x0[1]))
	print('Found minimum: x = {}, y = {}'.format(res[0],res[1]))

	print('Starting guesses are way off to illustrate actual root finding.\nResults are more precise, if guesses are spot on.')
	print('\n\n')

	

