from ode import *
from root import *
import matplotlib.pyplot as plt


	#Define Schrödinger equation as a coupled differential equation
def diff_eq(r,y,eps):
	f = y[1]
	ddf = -2*(1/r + eps)*y[0]
	return np.array([f,ddf])
	
	#Define auxiliary funcion 
def func_eps(eps,rmax=8,prob='B'):
		#Starting conditions
	r0 = 1e-3
	rstep = 1e-3
	y0 = np.asarray([r0-r0**2, 1-2*r0])
		#Differential equation for ode
	diff_this = lambda r,y: diff_eq(r,y,eps[0])
		#Ode 
	y_rmax,_,_ = driver(r0, rmax, rstep, y0, diff_this, rkstep34)
	if prob == 'B':
		return y_rmax[-1,0]
	else:
		return y_rmax[-1,0]-rmax*np.exp(-np.sqrt(-2*eps[0])*rmax)

def problemB():
	print('\nProblem B - Hydrogen atom')
	eps0 = -1
		#Root finder
	eps_min, n = newton([func_eps], [eps0], eps=1e-7)
	print('The lowest root found is eps_0 = {}\nThe exact result is eps_0 = -1/2'.format(eps_min[0]))
	print('The comparison between functions are found in figure ProblemBPlot.png\n It is a pretty good fit!')

	r0 = 1e-3
	rmax = 8
	rstep = 1e-3
	y0 = np.asarray([r0-r0**2, 1-2*r0])
		#Differential equation for ode
	diff_this = lambda r,y: diff_eq(r,y,eps_min[0])
		#Ode 
	y_rmax,r,_ = driver(r0, rmax, rstep, y0, diff_this, rkstep34)
		#plot
	fig,ax = plt.subplots(1,1)
	ax.plot(r,y_rmax[:,0],'r-',label='Found f')
	ax.plot(r,r*np.exp(-r),'b--',label='Exact f')
	ax.legend()
	fig.savefig('ProblemBPlot')
	print('\n\n')

def problemC():
	print('\nProblem C1 - investigate r_max dependence of eps_0\n')
	eps0 = -1
	r_vals = np.linspace(8,20,13)
		#Loop of rmax values
	eps_min = []
	for r in r_vals:
		root_this = lambda eps: func_eps(eps,r)
		e,n = newton([root_this],[eps0])
		print('r_max = {}, eps_0 = {}'.format(r,e[0]))

		root_this = lambda eps: func_eps(eps,r,'C')
		e,n = newton([root_this],[eps0])
		eps_min.append(e[0])

	print('\nProblem C2 - investigate precise boundary condition\n')
	for i in range(len(eps_min)):
		print('r_max = {}, eps_0 = {}'.format(r_vals[i],eps_min[i]))
	print('Taking these extra conditions into account increases the accuracy of the found epsilon_0.')
	print('\n\n')
