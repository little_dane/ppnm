import numpy as np
import matplotlib.pyplot as plt
import array
import random

"""
This class is for all the linear algebra functions
"""


"Matrix class"
class matrix(object):
	"Initialising the matrix"
	def __init__ (self, n:int, m:int, t:type='d'):
		self.size1 = n
		self.size2 = m
		self.data = array.array(t,[0,0]*(n*m))

	"Set matrix element [i,j]"
	def set(self, i:int, j:int, x):
		self.data[i+self.size1*j] = x

	"Get matrix element [i,j]"
	def get(self, i:int, j:int):
		return self.data[i+self.size1*j]

	"Get row i"
	def row(self, i):
		return [self.data[i+self.size1*j] for j in range(self.size2)]

	"Get row j"
	def col(self, j):
		return [self.data[i+self.size1*j] for i in range(self.size1)]

	"Set all matrix elements with nested list"
	def view(self, M):
		for i in range(self.size1):
			for j in range(self.size2):
				self.data[i+self.size1*j] = M[i][j]
	
	"Print matrix to stdout"
	def show(self):
		main = ''
		for i in range(self.size1):
			for j in range(self.size2):
				main += '{:.3f}'.format(self.data[i+self.size1*j]) + '\t'
			main += '\n'
		print(main)

	"Return matrix element [i,j]"
	def __getitem__ (self, ij):
		i,j = ij
		return self.get(i,j)

	"Set element [i,j]"
	def __setitem__ (self, ij, x):
		i,j = ij
		self.set(i, j, x)

"Copy matrix"
def copy(A):
	B = matrix(A.size1, A.size2)
	for i in range(A.size1):
		for j in range(A.size2):
			B[i,j] = A[i,j]
	return B

"Matrix-matrix multiplication"
def multiply(A:matrix, B:matrix):
	C = matrix(A.size1, B.size2)
	for i in range(C.size1):
		for j in range(C.size2):
			C[i,j] = sum([A[i,l]*B[l,j] for l in range(A.size2)])
	return C

"Matrix-vector multiplication"
def multiplyv(A:matrix, v:list):
	assert(A.size2 == len(v))
	b = []
	for i in range(len(v)):
		b.append(sum([A[i,j]*v[j] for j in range(len(v))]))
	return b

"Transpose matrix"
def trans(A:matrix):
	B = matrix(A.size2, A.size1)
	for i in range(A.size1):
		for j in range(A.size2):
			B[j,i] = A[i,j]
	return B

"Dot product of two vectors"
def dot(A:array, B:array):
	S = 0
	for i in range(len(A)):
		S += A[i]*B[i]
	return S

"QR decomposition of matrix"
def qr_gs_decomp(A:matrix):
	R = matrix(A.size2, A.size2)
	Q = matrix(A.size1, A.size2)
	Aj = copy(A)

	for i in range(A.size2):
		R[i,i] = dot(Aj.col(i), Aj.col(i))**(1./2)
		for l in range(A.size1):
			Q[l,i] = Aj[l,i]/R[i,i]
		for j in range(i+1, A.size2):
			R[i,j] = dot(Q.col(i), Aj.col(j))
			for l in range(A.size1):
				Aj[l,j] = Aj[l,j]-Q[l,i]*R[i,j]
	return Q,R

"Solve linear system from QR decomposition"
def qr_gs_solve(Q:matrix, R:matrix, b:list, check=0):
	x = [0 for i in range(Q.size2)]
	for i in range(Q.size2):
		x[i] = dot(Q.col(i),b)
	for i in range(Q.size2-1,-1,-1):
		x[i] /= R[i,i]
		for j in range(i-1,-1,-1):
			x[j] -= x[i]*R[j,i]
	return x

"Find inverse matrix from QR decomposition"
def qr_gs_inverse(Q:matrix, R:matrix):
	Rin = matrix(R.size1, R.size2)
	for i in range(R.size2-1,-1,-1):
		Rin[i,i] = 1/R[i,i]
		for l in range(i+1,R.size2):
			Rin[i,l] /= R[i,i]
		for j in range(i-1,-1,-1):
			for l in range(i,R.size2):
				Rin[j,l] -= R[j,i]*Rin[i,l]
	B = multiply(Rin, trans(Q))
	return B
