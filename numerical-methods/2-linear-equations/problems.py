import numpy as np
from linearequations import *

def problemA1():
	print('\nExercise A.1: \n')
		#Create random matrix
	A = matrix(4,3)
	dat = np.random.rand(4,3)*4
	A.view(dat)
	
	print('Starting matrix')
	A.show()
		#Using qr_gs_decomp
	Q,R = qr_gs_decomp(A)

	print('Is R upper triangular?')
	R.show()

	print('Is Q^TQ = 1? ')
	multiply(trans(Q),Q).show()

	print('Is QR = A?')
	print('A = ')
	A.show()
	print('QR = ')
	multiply(Q,R).show()
	print('Yes, QR = A')
	print('\n\n')

def problemA2():
	print('\nExercise A.2: \n')
		#Create random matrix
	A = matrix(3,3)
	dat = np.random.rand(3,3)*4
	A.view(dat)

	print('Starting matrix, A:')
	A.show()

	b = np.random.rand(3,1)*3
	print('Vector b:')
	for i in range(len(b)):
		print(float(b[i]))

		#Using qr_gs_decomp
	Q,R = qr_gs_decomp(A)
	print('\nQ = ')
	Q.show()

	print('R = ')
	R.show()
		#Using qr_gs_solve
	x = qr_gs_solve(Q,R,b)
	print('x = ')
	for i in range(len(x)):
		print(float(x[i]))
	
	print('Ax =')
	b2 = multiplyv(A,x)
	for i in range(len(b2)):
		print(float(b2[i]))
	print('Thus Ax = b')
	print('\n\n')

def problemB():
	print('\nTask B: \n')
		#Create random matrix
	A = matrix(4,4)
	dat = np.random.rand(4,4)*5-2.5
	A.view(dat)

	print('Starting matrix A:')
	A.show()

		#Using qr_gs_decomp
	Q,R = qr_gs_decomp(A)
		#Using qr_gs_inverse
	B = qr_gs_inverse(Q,R)
	
	print('Inverse matrix B:')
	B.show()

	print('Is AB = I?')
	print('AB =')
	multiply(A,B).show()
	print('BA = ')
	multiply(B,A).show()
	print('Yes, AB = I, where I is the indentity matrix')
	print('\n\n')
