import numpy as np

"""
	Plain Monte-Carlo integrator 
"""
def plain(f, a, b, N:int):
	volume = 1
	for i in range(len(a)):
		volume *= b[i]-a[i]
	
	sum1 = 0
	sum2 = 0

	for i in range(int(N)):
		fx = f(np.random.uniform(a,b,len(a)))
		sum1 += fx
		sum2 += fx*fx
	
	mean = sum1/N				# <f_i>
	sigma = np.sqrt(sum2/N-mean*mean)	# sigma^2 = <(f_i)^2> - <f_i>^2
	SIGMA = sigma/np.sqrt(N)		# SIGMA^2 = <Q^2> - <Q>^2

	return mean*volume,SIGMA*volume
