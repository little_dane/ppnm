import numpy as np
import matplotlib.pyplot as plt
from montecarlo import *
import matplotlib.patches as mpatches



def problemA():
	print('\nProblem A - Testing plain Monte Carlo integration\n')

	N = 1e6
	
	print('\nFirst interesting integral is sin(x)^2 from 0 to pi/2')
	print('The exact solution is pi/4 = {:.5f}'.format(np.pi/4))
	f = lambda x: np.sin(x[0])*np.sin(x[0])
	a = np.array([0])
	b = np.array([np.pi/2])

	(res,err) = plain(f,a,b,N)
	print('The calculated result is {:.5f}'.format(res))

	print('\nSecond interesting integral is the unit sphere in radial coordinates\n')
	print('The exact solution is 4*pi/3 = {:.5f}'.format(4*np.pi/3))
	g = lambda x: np.sin(x[1])*x[2]*x[2]
	a = np.array([0,0,0])
	b = np.array([2*np.pi,np.pi,1])
	
	(res,err) = plain(g,a,b,N)
	print('The calculated result is {:.5f}'.format(res))

	print('\nSolve difficult singular integral\n')
	print('The exact solution is 1.39320 at five digits')
	h = lambda x: (1-np.cos(x[0])*np.cos(x[1])*np.cos(x[2]))**(-1)/np.pi**3
	a = np.array([0,0,0])
	b = np.array([np.pi,np.pi,np.pi])

	(res,err) = plain(h,a,b,N)
	print('The calculated result is {:.5f}\n\n'.format(res))

def problemB():
	print('\nProblem B - check error\n')
	N = 1000

	f = lambda x: np.sin(x[0])*np.sin(x[0])
	a = np.array([0])
	b = np.array([np.pi/2])

		#Plot to compare monte carlo and 1/sqrt(N)
	fig,ax = plt.subplots(1,1)
	
	for i in range(100,N,1):
		(res,err) = plain(f,a,b,i)
		ax.plot(i,err,'b.',label='Monte Carlo')
		ax.plot(i,1/np.sqrt(i),'r.',label=r'$1/\sqrt{N}$')

	print('In figure ProblemBPlot.png, the error is shown as the blue dots and the red graph shows 1/sqrt(N).\nThey clearly show the same behaviour.\n\n')
	ax.set_xlabel('N')
	ax.set_title(r'Error of plain Monte Carlo compared to $1/\sqrt{N}$') 
	red = mpatches.Patch(color='red',label=r'$1/\sqrt{N}$')
	blue = mpatches.Patch(color='blue',label='Monte Carlo')
	plt.legend(handles=[red,blue])
	fig.savefig('ProblemBPlot')


