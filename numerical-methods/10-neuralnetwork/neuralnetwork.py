import numpy as np
from scipy.optimize import minimize


class neuralNetwork:
	"""
	Function (and derivative and anti-derivative) must be supplied by user
	"""
	def __init__(self,n,f):
		self.n = n
		self.f = f
		self.p = np.random.random(n*3) #Params initialised as random vector
						#[:n] = a, [n:2n] = b, [2n:] = w

	def feedforward(self,x):
		res = 0
		for i in range(self.n):
			res += self.f((x-self.p[i])/self.p[i+self.n])*self.p[i+2*self.n] #y = f((x-a)/b)*w
		return res

	def deviation(self,p,xs,ys):
		self.p = p
		res = 0
		for i in range(len(xs)):
			res += (self.feedforward(xs[i])-ys[i])**2 #(F(x)-y)**2
		return res

	def train(self,xs,ys):
		new_p = minimize(lambda p: self.deviation(p,xs,ys),self.p)
		self.p = new_p.x					

class neuralNetworkB:
	"""
	Function (and derivative and anti-derivative) must be supplied by user
	"""
	def __init__(self,n,f):
		self.n = n
		self.f = f
		self.p = np.random.random(n*5) #Params initialised as random vector
					#[:n] = a, [n:2n] = b, [2n:3n] = w1, [3n:4n] = w2, [4n:] = w3


	def feedforward(self,x,func='f'):
		res = 0
			#Choose between f, df and F
		if func == 'df':
			ftype = 3
		elif func == 'F':
			ftype = 4
		else:
			ftype = 2
		for i in range(self.n):
			res += self.f((x-self.p[i])/self.p[i+self.n])*self.p[i+ftype*self.n] #y = f((x-a)/b)*w
		return res


	def deviation(self,p,xs,ys,dfys,Fys):
		self.p = p
		res = 0
		for i in range(len(xs)):
			res += (self.feedforward(xs[i],'f')-ys[i])**2 #(F(x)-y)**2
			res += (self.feedforward(xs[i],'df')-dfys[i])**2 #same for derivative
			res += (self.feedforward(xs[i],'F')-Fys[i])**2  #same for anti-derivative
		return res

	def train(self,xs,ys,dfys,Fys):
		new_p = minimize(lambda p: self.deviation(p,xs,ys,dfys,Fys),self.p)
		self.p = new_p.x	
