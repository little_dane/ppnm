This Neural Network problem uses scipy's minimisation instead of my own for increased stability and precision.

Furthermore, due to a lot of neurons, it takes a while to fully compile (especially part B). If it takes too long, the number of neurons, n, can be decreased.
