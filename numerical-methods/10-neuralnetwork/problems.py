import numpy as np
import matplotlib.pyplot as plt
from neuralnetwork import *

def activationf(x):
	return np.exp(-x**2)

def problemA():
	print('\nProblem A - Develop a simple neural network\n')
		#Define neurons and network
	neurons = 15
	ann = neuralNetwork(neurons,activationf)

	xs = np.linspace(0,10,30)
	ys = np.cos(xs)
		#Training
	ann.train(xs,ys)

	res_x = np.linspace(1.1,2*np.pi,5)
	res_y = []
		#Feed forward
	for x in res_x:
		res_y.append(ann.feedforward(x))

	x = np.linspace(1,2*np.pi,100)
	y = np.cos(x)

	print('Activation function: exp(-x^2)')
	print('Number of hidden neurons: {}'.format(neurons))
	print('Network is trained with {} points'.format(len(xs)))
	print('Interpolating with {} points'.format(len(res_x)))
	print('Interpolation to cos(x) is found in figure ProblemAPlot.png\n\n')
		#Plot
	fig,ax = plt.subplots(1,1)
	ax.plot(x,y,'r--',label='cos')
	ax.plot(res_x,res_y,'b*',label='Interpolated points')
	ax.legend()
	ax.grid(True)
	fig.savefig('ProblemAPlot')


def problemB():
	print('\nProblem B - Neural Network also predicting (anti-)derivative\n')
		#Define neurons and network with deriv and anti-deriv
	n = 15
	ann = neuralNetworkB(n,activationf)
		#Define values
	f = lambda x: 2*np.sin(x/2)**2
	xs = np.linspace(0,10,30)
	ys = f(xs)
	df = lambda x: 2*np.sin(x/2)*np.cos(x/2)
	dfys = df(xs)
	F = lambda x: x-np.sin(x)
	Fys = F(xs)
		#Train
	ann.train(xs,ys,dfys,Fys)
	
	res_x = np.linspace(0,10,5)
	res_y = []
		#Feedforward with f, df and F in collective list
	for func in ['f','df','F']:
		for x in res_x:
			res_y.append(ann.feedforward(x,func))
		
	print('Activation function: exp(-x^2)')
	print('Number of hidden neurons: {}'.format(n))
	print('Network is trained with {} points'.format(len(xs)))
	print('Interpolating with {} points'.format(len(res_x)))
	print('Interpolations to 2sin(x/2)^2 and its derivative and antiderivative are found in figure ProblemBPlot.png\n\n')
		#Plot
	x = np.linspace(0,10,100)
	fig,ax = plt.subplots(1,3)
	ax[0].plot(x,f(x),'r--')
	ax[0].plot(res_x,res_y[:5],'b*')
	ax[0].set_title(r'$f(x) = 2\sin(3x)^2$')

	ax[1].plot(x,df(x),'r--',label='Analytical function')
	ax[1].plot(res_x,res_y[5:10],'b*',label='Found points')
	ax[1].set_title(r"$f'(x) = 12\sin(3x)\cos(3x)$")

	ax[2].plot(x,F(x),'r--')
	ax[2].plot(res_x,res_y[10:],'b*')
	ax[2].set_title(r'$F(x) = x-1/6\sin(6x)$')
	
	fig.tight_layout()
	fig.savefig('ProblemBPlot')
	
