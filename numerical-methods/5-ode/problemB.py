import numpy as np
import matplotlib.pyplot as plt
from ode import *

#Some parameters are set
N = 5806000 #according to Google's numbers for 2019
T_r = 10


def df(t,y,T_c):
	dy = []
	dy.append(-y[1]*y[0]/(N*T_c))
	dy.append(y[1]*y[0]/(N*T_c)-y[1]/T_r)
	dy.append(y[1]/T_r)
	return np.asarray(dy) 

"""
	S = susceptible
	I = infectious
	R = removed
	
	N = population size
	T_c = time between contacts
	T_r = recovery time
	
	T_r/T_c = typical number of new infected by one infectious individual
"""
def problem():
	print('\nProblem B - Corona-virus in Denmark')

	#Starting values for S, I and R
	I_0 = 100
	S_0 = N - I_0
	R_0 = 0.

	fig,axis = plt.subplots(2,2)


	print('The development of COVID-19 can be seen in figure ProblemBPlot.png')
	print('There are four figures for four different values of T_c. \n It is clear, that the higher T_c value, the fewer people are "removed" as there are less sick people in total.')

	T_c_vals = [1,2,5,10]
	for i,T_c in enumerate(T_c_vals):
		new_df = lambda t,y: df(t,y,T_c)
			#solving
		(y,t,n) = driver(0,250,1,[S_0,I_0,R_0],new_df,rkstep34)
			#Plot
		ax = axis[i//2,i%2]
		ax.plot(t,y[:,0]*1e-6,label = 'S')
		ax.plot(t,y[:,1]*1e-6,label = 'I')
		ax.plot(t,y[:,2]*1e-6,label = 'R')
		ax.legend()
		ax.set_title(r'$T_c$ = {0}'.format(T_c))
		ax.set_ylabel('Population [millions]')
		ax.set_xlabel('t [days]')

	fig.tight_layout()
	fig.savefig('ProblemBPlot')

	print('\n\n')
