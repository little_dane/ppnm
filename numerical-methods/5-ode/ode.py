import numpy as np


"""
	float a - start point
	float b - end point
	float h - initial step size
	list yt - starting parameters 
	list f - differential equations
	step - which type of stepper to use
	double acc - absolute accuracy goal
	double eps - relative accuracy goal
	int n - current step
"""
def driver(a, b, h, yt, f, stepper, acc = 1e-3, eps = 1e-3, n = 0):
	t = np.array([a])
	y_vals = np.array([yt])

	while b-t[-1] > 1e-6:
		n += 1; assert(n < 2000) #count all steps
		if t[-1]+h > b: #make sure that it maxes at b
			h = b-t[-1]
		yi,syi = stepper(t[-1], h, yt, f) #use the stepper
		ei = np.sqrt(np.inner(syi,syi)) #error
		toli = (eps*np.sqrt(np.inner(yi,yi))+acc)*np.sqrt(h/(b-a)) #tolerance

		if ei < toli:
			t = np.append(t, t[-1]+h)
			yt = yi
			y_vals = np.vstack((y_vals,np.array([yi])))
		h *= (toli/ei)**0.25*0.95
	return y_vals,t,n



def rkstep12(t: float, h:float, yt, f):
	k0 = f(t,yt)
	k1 = f(t+h,yt+h*k0)
	k = (k0+k1)/2

	y1 = yt + h*k
	sy1 = h*(k-k0)

	return y1,sy1

def rkstep34(t:float, h:float, yt, f):
	k0 = f(t,yt)
	k1 = f(t+0.5*h,yt+0.5*h*k0)
	k2 = f(t+0.5*h,yt+0.5*h*k1)
	k3 = f(t+h,yt+h*k2)
	k_high = k0/6 + k1/3 + k2/3 + k3/6

	k4 = f(t+h,yt+h*k0)
	k_low = k0/6 + 4*k1/6 + k4/6

	y1 = yt + h*k_high
	sy1 = h*(k_high-k_low)

	return y1,sy1

