import numpy as np
import matplotlib.pyplot as plt
from ode import *

"For finding the fraction (r_a-r_b)/(abs(r_a-r_b)**3)"
def rvector(r_a,r_b):
	x = r_a[0]-r_b[0]
	y = r_a[1]-r_b[1]
	l = np.sqrt(x**2+y**2)**3
	return np.asarray([x/l,y/l])

"Differential equation"
def df(t,y):
		#Splitting y into three objects
	y_1 = list(y[:4])
	y_2 = list(y[4:8])
	y_3 = list(y[8:])
		#Finding accelerations
	a_1 = - rvector(y_1[:2],y_2[:2]) - rvector(y_1[:2],y_3[:2])
	a_2 = - rvector(y_2[:2],y_3[:2]) - rvector(y_2[:2],y_1[:2])
	a_3 = - rvector(y_3[:2],y_1[:2]) - rvector(y_3[:2],y_2[:2])
		#New values 
	ny_1 = y_1[2:]+list(a_1)
	ny_2 = y_2[2:]+list(a_2)
	ny_3 = y_3[2:]+list(a_3)
		#Gather values in one list
	ny = ny_1+ny_2+ny_3

	return np.asarray(ny)
	


def problem():
	print('\nProblem C - Newtonian gravitational three-body problem\n')
		#Define starting values for the three objects in the order [x_0, y_0, vx_0, vy_0]
	y_1 = [-0.97000436,0.24308753,0.4662036850,0.43236573]
	y_2 = [0,0,-0.93240737,-0.86473146]
	y_3 = [0.97000436,-0.24308753,0.4662036850,0.43236573]
		#Append all objects to collected list
	y = y_1+y_2+y_3

	T = np.pi 
	step = 1
	(y,t,n) = driver(0,T,step,np.asarray(y),df,rkstep34)

	print('The three body plot is found in ProblemCPlot.png')
	print('The total time is pi, so all three paths can be seen tracing the 8 figure.')
		#Plot
	fig,ax = plt.subplots(1,1)
	ax.plot(y[:,0],y[:,1],label='Object 1',alpha=0.5)
	ax.plot(y[:,4],y[:,5],label='Object 2',alpha=0.5)
	ax.plot(y[:,8],y[:,9],label='Object 3',alpha=0.5)
	ax.legend()
	ax.set_title('Figure 8 solution')
	fig.savefig('ProblemCPlot')

	print('\n\n')

	
