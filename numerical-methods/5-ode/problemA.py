import numpy as np
import matplotlib.pyplot as plt
from ode import *

#I chose to solve the system of a dampened harmonic oscillator for problem A

"The differential equation of a harmonic oscillator"
def osc(t, y, m, k):
	df = np.array([y[1], -k/m*y[0]])
	return df

"The differential equation of a dampened harmonic oscillator"
def osc_damp(t, y, m, k, c):
	df = np.array([y[1], -c/m*y[1]-k/m*y[0]])
	return df

def problem():
	print('\nProblem A - Solve a system of differential equations\n')
	print('The solution to a dampened harmonic oscillator can be seen in figure ProblemAPlot.png\n')
	fig,ax = plt.subplots(2,1) #for plotting
	
	"Conditions"
	a = 0.
	b = 10.
	h = 0.2
	yt = np.array([1,0])
	
	print('Solving: \n m x\'\' + l x\' + k x = 0, \n where m=k=1 and l is varied in the interval:\n t = [{},{}]\n '.format(a,b))

	for l in np.array(range(5))/4.:
		print('l={}'.format(l))
		f = lambda t,y: osc_damp(t,y,1,1,l)
		y,t,n = driver(a,b,h,yt,f,rkstep34)
		print('Solved in {} steps\n'.format(n))

		ax[0].plot(t,[y[i][0] for i in range(len(y))], '--', label='l={}'.format(l))
		ax[1].plot(t,[y[i][1] for i in range(len(y))], '--', label='l={}'.format(l))
		
		#Plot
	ax[0].legend()
	ax[0].set_ylabel(r'$x(t)$')
	ax[1].set_ylabel(r'$\dot{x}(t)$')
	ax[1].set_xlabel(r'$t$')
	ax[0].grid(True)
	ax[1].grid(True)
	ax[0].set_title('Dampened harmonic oscillator')

	plt.savefig('ProblemAPlot')

	print('\n\n')

