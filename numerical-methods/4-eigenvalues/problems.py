from eigen import *
import random
import matplotlib.pyplot as plt
import time

#creates a random, symmetrical matrix of order n
def randomMatrix(n):
	A = matrix(n,n)
	for i in range(n):
		A[i,i] = random.uniform(-10,10)
		for j in range(i,n):
			c = random.uniform(-10,10)
			A[i,j] = c
			A[j,i] = c

	return A


def problemA1():
	print('\nExercise A1 - Jacobi diagonalisation with cyclic sweeps\n')
	n = 6  
	A = randomMatrix(n)
	print('A = ')
	A.show()

	D = copy(A)
	(e,V) = cyclic(D)
	print('Eigenvectors:')
	V.show()
	print('Diagonalised eigenvalue matrix, D: ')
	e.show()
	
	print('V^T A V = ')
	multiply(trans(V), multiply(A,V)).show()
	print('Thus V^T A V = D')
	print('\n\n')
def problemA2():
	print('\nExercise A2 - Quantum particle in a box\n')	
	"Creating the Hamiltonian"
	n = 21
	m = int(n/3)
		#Creating the Hamiltonian
	s = 1.0/(n+1)
	H = matrix(n,n)
	for i in range(n-1):
		 H[i,i] = -2
		 H[i,i+1] = 1
		 H[i+1,i] = 1
	H[n-1,n-1] = -2
	H = scale(H,-1/s/s)

	(e,V) = cyclic(H)
	print('The first 10 eigenvalues are:\n')
		#Printing found eigenvalues
	for k in range(m):
		exact = np.pi*np.pi*(k+1)*(k+1)
		calculated = e[k,k]
		print('E_k: {}, exact: {}, calculated: {}'.format(k,exact,calculated))

	print('\nThe first five eigenfunctions are shown in figure Particle.png')
	print('It is clear that they fit the analytical descriptions.\n')
	
	t = np.linspace(0,1,n)
		#Plot
	fig,ax = plt.subplots(1,1)
	ax.plot(t,V.col(0),label = 'Eigenfunction 1')
	ax.plot(t,V.col(1),label = 'Eigenfunction 2')
	ax.plot(t,V.col(2),label = 'Eigenfunction 3')
	ax.grid(True)
	ax.legend()
	ax.set_title('Particle in a box - eigenfunctions')
	fig.savefig('Particle')
	print('\n\n')
	
		
def problemB():
	print('\nExercise B - Eigenvalue-by-eigenvalue\n')

	t_c = []
	t_v = []
	t_f = []
	x = []
		#Timing
	for i in range(2,15,1):
		A = randomMatrix(i)
			#Timing cyclic	
		a = time.time()
		cyclic(copy(A))
		b = time.time()
		t_c.append(b-a)
			#Timing value by value - lowest eigenvalue
		a = time.time()
		value(copy(A),1)
		b = time.time()
		t_v.append(b-a)
			#Timing value by value - full diagonalisation
		a = time.time()
		value(copy(A),i)
		b = time.time()
		t_f.append(b-a)
		x.append(i)

	print('The timings can be seen in figure PlotB.png')
	print('It is clear, that the cyclic method scales as O(n^3).')
	print('The value by values function is faster when it finds the lowest eigenvalue than when it finds all.')


		#Checking eigenvalues
	A = randomMatrix(10)
	print('\nA random matrix A has been made: ')
	A.show()
	print('Using the cyclic method, the eigenvalues are: ')
	(e,V) = cyclic(A)
	es = []
	for i in range(e.size1):
		es.append(e[i,i])
	print(es)

	A1 = copy(A)
	A2 = copy(A)
		#High-to-low and low-to-high
	(e1,V1) = value(A1,3)
	(e2,V2) = value(A2,3,True)
	print('Using the value by value function on A, the three highest and lowest eigenvalues can be found.')
	print('The three highest: ')
	es1 = []
	for i in range(3):
		es1.append(e1[i,i])
	print(es1)
	print('The three lowest: ')
	es2 = []
	for i in range(3):
		es2.append(e2[i,i])
	print(es2)
	print('This was done by inserting "True" as a parameter in the value-by-value method, so it  could be transposed.')
	print('\n\n')

		#Plot
	fig,ax = plt.subplots(1,1)
	ax.plot(x,t_c,'r.',label='cyclic')
	ax.plot(x,t_v,'b.',label='lowest')
	ax.plot(x,t_f,'g.',label='fully')
	ax.set_xlabel('Number of eigenvalues')
	ax.set_ylabel('Time')
	ax.grid(True)
	ax.legend()
	fig.savefig('PlotB')
	
	

