from linearequations import *

"""
	Quasi-Newton minimisation method
		user must supply gradient
"""

def qnewton(f, x_0, df, eps=1e-6, alpha=1e-4,n=0):
	#First definitions and creation of the inverse Hessian matrix B
	x = np.copy(np.array(x_0))
	B = matrix(len(x),len(x))
	dB = copy(B) 
	for i in range(len(x)):
		B[i,i] = 1

	while inner_v(df(x)) > eps:
		assert(n<5e4)
		n += 1
		dx = np.array(multiplyv(B,-np.array(df(x))))
		lam = 1.0 
		dxdf = dot(dx,df(x))
		while True:	#Backtracking
			n += 1
			lam /= 2.0
			if f(x+lam*dx) < f(x) + alpha*lam*dxdf:	#Armijo condition
				break
			if lam < 1/64.:
				B = matrix(len(x),len(x))
				for i in range(len(x)):
					B[i,i] = 1
				break
			#y, u and c according to equation (12)
		y = np.array(df(x+lam*dx))-np.array(df(x))
		u = lam*dx - np.array(multiplyv(B,y))
		c = u/dot(lam*dx,y)
			#Broyden's update
		for i in range(len(c)):
			for j in range(len(dx)):
				dB[i,j] = c[i]*lam*dx[j]
		B.add(dB)
		x = x + lam*dx

	return x,n


