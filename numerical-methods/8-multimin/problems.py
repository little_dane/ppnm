import numpy as np
from minimisation import *

		#Function and gradient definitions
"""
	Define Rosenbrock valley function 
		and its gradient below
"""
def rosenbrock(params):
	return (1-params[0])**2 + 100*(params[1]-params[0]**2)**2
def rosenbrock_grad(params):
	x = -2+2*params[0]-400*params[0]*params[1]+400*params[0]**3
	y = 200*(params[1]-params[0]**2)
	return [x,y]

"""
	Define Himmelblau function 
		and its gradient below
"""
def himmelblau(params):
	return (params[0]**2+params[1]-11)**2 + (params[0]+params[1]**2-7)**2
def himmelblau_grad(params):
	x = 4*params[0]*(params[0]**2+params[1]-11) + 2*(params[0]+params[1]**2-7)
	y = 2*(params[0]**2+params[1]-11 + 4*params[1]*(params[0]+params[1]**2-7))
	return [x,y]


"""
	Define Breit-Wigner function
		and deviation function
			and its gradient below
"""
def breitwigner(E,m,gamma,A):	
	return A/((E-m)**2+gamma**2/4)

def deviation(params,E,sigma,err):

	m = params[0]
	gamma = params[1]
	A = params[2]


	res = 0
	for i in range(len(E)):
		res += (breitwigner(E[i],m,gamma,A) - sigma[i])**2./pow(err[i],2)
	return res

def deviation_grad(params,E,sigma,err):
	
	m = params[0]
	gamma = params[1]
	A = params[2]
	
	x = 0
	y = 0
	z = 0

	for i in range(len(E)):
		x += (4*A*(E[i]-m)*(A/(gamma**2/4+(E[i]-m)**2)-sigma[i]))/((gamma**2/4+(E[i]-m)**2)**2)/pow(err[i],2)
		y += -(A*gamma*(A/(gamma**2/4+(E[i]-m)**2)-sigma[i]))/((gamma**2/4+(E[i]-m)**2)**2)/pow(err[i],2)
		z += (2*(A/(gamma**2/4+(E[i]-m)**2)-sigma[i]))/(gamma**2/4+(E[i]-m)**2)/pow(err[i],2)

	return [x,y,z]




def problemA():
	print('\nProblem A - Testing my Quasi-Newton minimisation method\n')

	print("\nFirst, Rosenbrock's valley function.")

	(x, steps) = qnewton(rosenbrock,np.array([0,0]),rosenbrock_grad)
	
	print('Expected minimum: (1,1).')
	print('Found minimum: ({},{}).'.format(x[0],x[1]))
	print('This was done in {} steps.'.format(steps))


	print("\nSecond, Himmelblau's function.")

	(x, steps) = qnewton(himmelblau,np.array([0,0]),himmelblau_grad)

	print('Expected minimum: (3,2).')
	print('Found minimum: ({},{}).'.format(x[0],x[1]))
	print('This was done in {} steps.\n\n'.format(steps))

def problemB():
	print('\nProblem B - Higgs discovery\n')
		#Define data
	E = np.array([101,103,105,107,109,111,113,115,117,119,121,123,125,127,129,131,133,135,137,139,141,143,145,147,149,151,153,155,157,159])
	sigma = np.array([-0.25,-0.30,-0.15,-1.71,0.81,0.65,-0.91,0.91,0.96,-2.52,-1.01,2.01,4.83,4.58,1.26,1.01,-1.26,0.45,0.15,-0.91,-0.81,-1.41,1.36,0.50,-0.45,1.61,-2.21,-1.86,1.76,-0.50])
	err = np.array([2.0,2.0,1.9,1.9,1.9,1.9,1.9,1.9,1.6,1.6,1.6,1.6,1.6,1.6,1.3,1.3,1.3,1.3,1.3,1.3,1.1,1.1,1.1,1.1,1.1,1.1,1.1,0.9,0.9,0.9])

	start = np.array([125.9,2,6])

	dev = lambda params: deviation(params,E,sigma,err)
	dev_grad = lambda params: deviation_grad(params,E,sigma,err)

	(x, steps) = qnewton(dev,start,dev_grad)
	print('Expected values are mass = 125.3 GeV and gamma = 4 MeV.')
	print('Fitting the Breit-Wigner function  yielded parameters: m = {}, gamma = {}, A = {}.'.format(x[0],x[1],x[2]))
	print('This was done in {} steps.\n\n'.format(steps))
	
