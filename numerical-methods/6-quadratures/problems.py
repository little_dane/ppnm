import numpy as np
import scipy.integrate as sp
import math
from integrator import *

def problemA():
	print('\n\nProblem A - Testing recursive integration\n\n')
	
	fs = [lambda x: np.sqrt(x), lambda x: 4*np.sqrt(1-x**2)]
	Qs = [integrator(fs[0],0,1), integrator(fs[1],0,1)]
	
	print('Calulate integral of sqrt(x) from 0 to 1\n')		
	print('Actual result is 2/3\n')
	print('Calculated result is {}\n'.format(Qs[0][0]))
	print('This pretty close.\n\n')

	print('Calculate integral of 4*sqrt(1-x²) from 0 to 1\n')
	print('Actual result is pi\n')
	print('Calculated result is {}\n'.format(Qs[1][0]))
	print('This is pretty close.')
	print('\n\n')

def problemB():
	print('\n\nProblem B - Check Clenshaw-Curtis transformation')
	print('As this is done in Python, integration values are compared to scipy.integrate instead of the supplied o8av from C#\n\n')
	fs = [lambda x: 1/np.sqrt(x), lambda x: np.log(x)/np.sqrt(x), lambda x: 4*np.sqrt(1-x**2)]
	Q1, err1, steps1 = integrator(fs[0],0,1)
	Q2, err2, steps2 = clenshawCurtis(fs[0],0,1)
	Qs, errs = sp.quad(fs[0],0,1)
	print('First, integrate 1/sqrt(x) from 0 to 1 which is equal to 2\n')
	print('Without Clenshaw-Curtis: value = {}, error = {}, steps = {}'.format(Q1,err1,steps1))
	print('With Clenshaw-Curtis: value = {}, error = {}, steps {}'.format(Q2,err2,steps2))
	print('Using Python integration through scipy module: value = {}, err = {}'.format(Qs,errs))



	Q1, err1, steps1 = integrator(fs[1],0,1)
	Q2, err2, steps2 = clenshawCurtis(fs[1],0,1)
	Qs, errs = sp.quad(fs[1],0,1)
	print('\nNext, integrate ln(x)/sqrt(x) from 0 to 1 which is equal to -4\n')
	print('Without Clenshaw-Curtis: value = {}, error = {}, steps = {}'.format(Q1,err1,steps1))
	print('With Clenshaw-Curtis: value = {}, error = {}, steps {}'.format(Q2,err2,steps2))
	print('Using Python integration through scipy module: value = {}, err = {}'.format(Qs,errs))
	
	
	Q1, err1, steps1 = integrator(fs[2],0,1)
	Q2, err2, steps2 = clenshawCurtis(fs[2],0,1)
	Qs, errs = sp.quad(fs[2],0,1)
	print('\nLast, integrate 4*sqrt(1-x^2) from 0 to 1 which is equal to pi\n')
	print('Without Clenshaw-Curtis: value = {}, error = {}, steps = {}'.format(Q1,err1,steps1))
	print('With Clenshaw-Curtis: value = {}, error = {}, steps {}'.format(Q2,err2,steps2))
	print('Using Python integration through scipy module: value = {}, err = {}'.format(Qs,errs))
	print('\n\n')


def problemC():
	print('\n\nProblem C - Testing infinite limits')
	print('Again, integration values are compared to scipy.integrate instead of the supplied o8av from C#\n\n')

	print('To test infinite limits the integral of exp(-x^2) is taken\n')
	print('First, exp(-x^2) from -inf to inf. This is equal to sqrt(pi)'
)
	f = lambda x: np.exp(-x**2)

	Q,err,steps = integrator(f,-np.inf,np.inf)
	Qs,errs = sp.quad(f,-np.inf,np.inf)
	print('My implementation: value = {}, error = {}, steps = {}'.format(Q,err,steps))
	print("Scipy's integration: value = {}, error = {}".format(Qs,errs))

	print('\nNow, exp(-x^2) from -inf to 0. This is equal to sqrt(pi)/2')
	Q,err,steps = integrator(f,-np.inf,0)
	Qs,errs = sp.quad(f,-np.inf,0)
	print('My implementation: value = {}, error = {}, steps = {}'.format(Q,err,steps))
	print("Scipy's integration: value = {}, error = {}".format(Qs,errs))

	print('\nLast, exp(-x^2) from 0 to inf. This is equal to sqrt(pi)/2')
	Q,err,steps = integrator(f,0,np.inf)
	Qs,errs = sp.quad(f,0,np.inf)
	print('My implementation: value = {}, error = {}, steps = {}'.format(Q,err,steps))
	print("Scipy's integration: value = {}, error = {}".format(Qs,errs))

	print("\nMy implementation is pretty close but not as good as Scipy's integration")
	print('\n\n')

