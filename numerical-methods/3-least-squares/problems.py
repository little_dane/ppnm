import numpy as np
import math
import matplotlib.pyplot as plt
from leastsquares import *

def one(x):
	return 1
def con(x):
	return x
def sqa(x):
	return x*x



def problems():
	print('\nProblem A and B\n')
		#Define data
	t = [1, 2, 3, 4, 6, 9, 10, 13, 15]
	y = [117, 100, 88, 72, 53, 29.5, 25.2, 15.2, 11.1]
	dy = [117/20, 100/20, 88/20, 72/20, 53/20, 29.5/20, 25.2/20, 15.2/20, 11.1/20]

	f = [one, con]

	lny = [math.log(y[i]) for i in range(len(y))]
	lndy = [dy[i]/y[i] for i in range(len(dy))]
		#Using lsfit function
	result, cov = lsfit(t, lny, lndy, f)

	print('Fitted parameters')
	print(result)
	print('Covariance matrix')
	for i in range(cov.size1):
		for j in range(cov.size2):
			print('cov[{:1d},{:1d}] = {:.6f}'.format(i,j,cov[i,j]))
	print('Half-life is: {:.1f} pm {:.1f}'.format(np.log(2)/result[1],np.log(2)/np.power(result[1],2)*np.sqrt(cov[1,1])))
	print('According to wikipedia, the half-life of Ra-224 is 3.6 years, which is within the calculated uncertainties.')
	print('Figure is found in plot.png')
	n = 50
	tt = [t[0]+i*t[-1]/n for i in range(n)]
	yy = [np.exp(result[0]+result[1]*tt[i]) for i in range(n)]
	yyu = [np.exp((result[0]+np.sqrt(cov[0,0])) + (result[1]+np.sqrt(cov[1,1]))*tt[i]) for i in range(n)] 
	yyl = [np.exp((result[0]-np.sqrt(cov[0,0])) + (result[1]-np.sqrt(cov[1,1]))*tt[i]) for i in range(n)]
		#Plot
	figure,ax = plt.subplots(1,1)
	ax.errorbar(t,y,yerr=dy,fmt='b.',label='Dataset',zorder=5)
	ax.plot(tt,yy,'r-',label='Fit')
	ax.plot(tt,yyu,'k--',label='Uncertainty of fit')
	ax.plot(tt,yyl,'k--')
	ax.grid(True)
	ax.set_axisbelow(True)
	ax.legend()
	ax.set_xlabel('Time in Days')
	ax.set_ylabel('Activity')
	ax.set_title('Activity of ThX')
	plt.savefig('plot')
	print('\n\n')



