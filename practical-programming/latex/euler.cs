using System;
using static System.Math;

static class nonef{
	public static vector euler(double L){
		int a = 0;

		Func<double, double> SPrime = (s) => Sin(Pow(s,2));
		Func<double, double> CPrime = (s) => Cos(Pow(s,2));

		double x = quad.o8av(CPrime, a, L);
		double y = quad.o8av(SPrime, a, L);

		return new vector(x,y);
			
	}
}
