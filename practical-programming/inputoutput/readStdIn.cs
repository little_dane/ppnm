using System;
class readStdIn{
	static void Main(){
		System.IO.TextReader stdin = Console.In;
		System.IO.TextWriter stdout = Console.Out;

		Console.WriteLine("\n B:");
		string s = stdin.ReadLine();
		string[] ws = s.Split(' ',',','\t');
		foreach(var w in ws){
			double x = double.Parse(w);
			stdout.WriteLine("{0} {1} {2}", x, Math.Sin(x), Math.Cos(x));
		}
	}
}

