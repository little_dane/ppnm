using System;
class readInputFile{
	static void Main(){
		System.IO.StreamReader input = new System.IO.StreamReader("input2.txt");
		System.IO.StreamWriter output = new System.IO.StreamWriter("out.txt",append:true);

		output.WriteLine("\n C:");
		do{
			string s = input.ReadLine();
			if(s == null){break;}
			string[] ws = s.Split(' ',',','\t');
			foreach(var w in ws){
				double x = double.Parse(w);
				output.WriteLine("{0} {1} {2}", x, Math.Sin(x), Math.Cos(x));
			}
		}
		while(true);
		output.Close();
	}
}

