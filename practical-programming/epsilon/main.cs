using static System.Console;
using static System.Math;
using static approxfun;

class main{
	static void Main(){
		int i = 1;
		while(i + 1 > i){i++;}
		Write($"My max int = {i}\n");
		Write($"C#'s max int = {int.MaxValue}\n\n");

		i = 1;
		while(i - 1 < i){i--;}
		Write($"My min int = {i}\n");
		Write($"C#'s min int = {int.MinValue}\n\n");

		double x = 1;
		while(1 + x != 1){x /= 2;}
		Write($"My double machine epsilon = {x *= 2}\n");
		Write($"C#'s double machine epsilon ≃ {Pow(2,-52)}\n\n");

		float y = 1F;
		while(1F + y != 1F){y /= 2F;}
		Write($"My float machine epsilon = {y *= 2F}\n");
		Write($"C#'s float machine epsilon ≃ {Pow(2,-23)}\n\n");

		int max = int.MaxValue / 2;
		
		float float_sum_up = 1f;
		for(i = 2; i <= max; i++){float_sum_up += 1f / i;}

		float float_sum_down = 1f / max;
		for(i = max - 1; i > 0; i--){float_sum_down += 1f / i;}

		Write($"Float up = {float_sum_up}\n");
		Write($"Float down = {float_sum_down}\n\n");

		double double_sum_up = 1;
		for(i = 2; i <= max; i++){double_sum_up += 1 / i;}

		double double_sum_down = 1 / max;
		for(i = max - 1; i > 0; i--){double_sum_down += 1 / i;}

		Write($"Double up = {double_sum_up}\n");
		Write($"Double down = {double_sum_down}\n\n");

		Write($"3 and 3 are equal: {approx(3,3)}\n");
		Write($"3 and 4 are equal: {approx(3,4)}\n");
	}
}

