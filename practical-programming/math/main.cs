using System;
using static System.Console;
using static System.Math;
using static cmath;

class main {
	static int Main() {
		Write("Part A\n");

		complex i = new complex(0,1);	
		complex i_pi = new complex(0,PI);	

		Write("Calculate sqrt(2)\n");
		Write($"sqrt(2) = {sqrt(2)}\n");
		Write("Yay! Correct!!\n\n");

		Write("Calculate exp(i)\n");
		Write($"exp(i) = {cmath.pow(E,i)}\n");		
		Write("Yay! Correct!!\n\n");

		Write("Calculate exp(i*pi)\n");
		Write($"exp(i*pi) = {cmath.pow(E,i_pi)}\n");
		Write("1.22e-16 is approximately 0 ... so yay! Correct!!\n\n");

		Write("Calculate i^i\n");
		Write($"i^i = {cmath.pow(i,i)}\n");
		Write("Yay! Correct!!\n\n");

		Write("Calculate sin(i*pi)\n");
		Write($"sin(i*pi) = {cmath.sin(i_pi)}\n");
		Write("Yay! Correct!!\n\n");

		Write("Part B\n");	
		Write("Calculate sinh(i)\n");
		Write($"sinh(i) = {sinh(i)}\n");
		Write("Yay! Correct!!\n\n");

		Write("Calculate cosh(i)\n");
		Write($"cosh(i) = {cosh(i)}\n");
		Write("Yay! Correct!!\n\n");
	
		Write("Calculate sqrt(-1)\n");
		Write($"sqrt(-1) = {sqrt(new complex(-1,0))}\n");
		Write("Yay! Correct!!\n\n");

		Write("Calculate sqrt(i)\n");
		Write($"sqrt(i) = {sqrt(i)}\n");
		Write("Yay! Correct!!\n\n");


		return 0;
	}

	
}
