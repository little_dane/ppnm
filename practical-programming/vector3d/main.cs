class main{
	public static int Main(){
		double c = 2;
		vector3d v = new vector3d(1,2,3);
		vector3d u = new vector3d(4,5,6);
		
		v.print("v = ");
		u.print("u = ");

		(v+u).print($"v + u = ");
		(v-u).print($"v - u = ");

		(v*c).print($"v * {c} = ");
		(c*v).print($"{c} * v = ");

		System.Console.Write("v * u = {0}\n",v.dot_product(u));
		(v.vector_product(u)).print("v x u = ");
		System.Console.Write("|v| = {0}\n",v.magnitude());

		return 0;
	}
}
