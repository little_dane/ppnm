using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
//Defining infinity and NaN in a different class to try this method
static class Const{
	public const double inf = System.Double.PositiveInfinity;
	public const double nan = System.Double.NaN;
}

class main{
	static void Main(){
		Write("Exercise A.\n");

		Write("Log(x)/Sqrt(x) from 0 to 1:\n");
		Func<double,double> f1 = (x) => Log(x) / Sqrt(x);
		double a = 0, b = 1;
		double result1 = quad.o8av(f1, a, b);
		Write("{0}\t-4\n\n", result1);

		Write("Exp(-x^2) from -inf to inf:\n");
		Func<double,double> f2 = (x) => Exp(-Pow(x,2));
		a = -Const.inf; b = Const.inf;
		double result2 = quad.o8av(f2,a,b);
		Write("{0}\t{1}\n\n", result2, Sqrt(PI));

		Write("(ln(1/x))^p from 0 to 1:\n");
		for(double p = 0; p < 5.5; p += 0.5){
			a = 0; b = 1;
			Func<double,double> f3 = (x) => Pow(Log(1 / x), p);
			double result3 = quad.o8av(f3,a,b);
			Write("{0:f7}\t{1:f7}\tp = {2}\n",result3, gamma(p + 1),p);
		}



		Write("\n Exercise B.\n");
		Write("x^2 * Exp(-a*x^2) (with a = 1):\n");
		Func<double,double> f4 = (x) => Pow(x,2) * Exp(-Pow(x,2));
		a = 0; b = Const.inf;
		double result4 = quad.o8av(f4,a,b);
		Write("{0:f7}\t{1:f7}\n\n", result4, (1.0 / 4) * Sqrt(PI));

		Write("x/(Exp(x)-1):\n");
		Func<double,double> f5 = (x) => x / (Exp(x) - 1);
		double result5 = quad.o8av(f5,a,b);
		Write("{0:f7}\t{1:f7}\n\n", result5, Pow(PI,2) / 6);

		Write("x^(-x)\n");
		Func<double,double> f6 = (x) => Pow(x,-x);
		double result6 = quad.o8av(f6,0,1);
		Write("{0:f7}\t{1:f7}\n", result6, 1.29128599706266);


//Tried out lambdas for fun :)
		Write("\nExercise C.\n");
		Func<double, double> E = (alpha) => {
			Func<double, double> psi_norm_square = (x) => Exp(-alpha*x*x);
			Func<double, double> hamilton_exp_fun = (x) => (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*Exp(-alpha*x*x);
			double overlap = quad.o8av(psi_norm_square, -Const.inf, Const.inf);
			double hamilton_exp_value = quad.o8av(hamilton_exp_fun, -Const.inf, Const.inf);
			return hamilton_exp_value/overlap;
		};
		Write("Plot of E(alpha) can be seen in C.svg)\n");
		Write("E(1) = {0:f16}\n", E(1));
		Write("This corresponds to the speculated minimum at 0.5.\n");
//Creating the streamwriter with data from E(alpha)
		var outfile = new System.IO.StreamWriter("out.C.txt");
		for(double alpha = 1/16; alpha <= 3; alpha+=1.0/32) {
			outfile.Write("{0:f16} {1:f16}\n", alpha, E(alpha));
		}
		outfile.Close();
	}
//Gamma function written down here
	static int ncalls;
	static double gamma(double z){
		const double inf = System.Double.PositiveInfinity;
		if(z < 0){return -PI / Sin(PI * z) / gamma(z + 1);}
		if(z < 1){return gamma(z + 1) / z;}
		if(z > 2){return gamma(z - 1) * (z - 1);}
		Func<double,double> f = delegate(double x){
			ncalls++;
			return Pow(x, z - 1) * Exp(-x);
		};
		ncalls = 0;
		return quad.o8av(f, 0, inf);
	}

}
