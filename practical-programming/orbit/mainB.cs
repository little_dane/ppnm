using System;
using static System.Console;
using static System.Math;
using System.Collections;
using System.Collections.Generic;

class mainB{

	static int Main(string[] args){

	double N = 6;
	double phia = 0;
	double phib = 2*N*PI;
	double u_0 = 1;
	double e = 0;
	double uPrime_0 = 1e-3;

	foreach(string s in args){
		string[] ws = s.Split('=');
		if(ws[0] == "a") phia = double.Parse(ws[1]);
		if(ws[0] == "u_0") u_0 = double.Parse(ws[1]);
		if(ws[0] == "uPrime_0") uPrime_0 = double.Parse(ws[1]);
		if(ws[0] == "b") phib = double.Parse(ws[1]);
		if(ws[0] == "e") e = double.Parse(ws[1]);
		if(ws[0] == "N") N = double.Parse(ws[1]);	
	}
	
	vector ua = new vector(u_0, uPrime_0);

	List<double> phis = new List<double>();
	List<vector> us = new List<vector>();

	Func<double, vector, vector> uPrime = delegate(double x, vector y){
		return new vector(y[1], 1 + e*Pow(y[0],2) - y[0]);
	};

	vector ub = ode.rk23(uPrime, phia, ua, phib, phis, us, acc:1e-6, eps:1e-5, h:1e-5, limit: 10000);

	for(int i = 0; i < phis.Count; i++){
		Write($"{phis[i]}   {us[i][0]}\n");
	}


	return 0;

	}

}
