using System;
using static System.Math;
using static System.Console;
using System.Collections;
using System.Collections.Generic;

class mainA{

	static int Main(){
	//Defining the giving parameters
	double x0 = 0;
	double x1 = 3;
	vector y0 = new vector(0.5);
	//Defining the function
	Func<double, vector, vector> yPrime = delegate(double x, vector y){
		return new vector(y[0]*(1-y[0]));
	};
	
	List<double> xs = new List<double>();
	List<vector> ys = new List<vector>();
	//Calculation using ode
	vector y1 = ode.rk23(yPrime, x0, y0, x1, xs, ys);

	//Now to compare to the analytic results
	double v = 0;
	for(int i = 0; i < xs.Count; i++){
		v = 1 / (1 + Exp(-xs[i]));
		Write($"{xs[i]}   {ys[i][0]}   {v}\n");
	}
	
	return 0;
	
	}

}
