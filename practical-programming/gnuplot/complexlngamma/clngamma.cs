using static System.Console;
using static System.Math;
using static cmath;
using static complex;

class clngamma{
	static void Main(){
		for(int x = -2; x <= 2; x += 1){
			for(int y = -2; y <= 2; y += 1){
				complex z = new complex(x,y);
				WriteLine("{0,8:N0}\t{1,8:N0}\t{2,8:N5}\t{3,8:N5}", z.Re, z.Im, lngam(z).Re, lngam(z).Im);
			}
		}
	}

	private static complex lngam(complex z){
		return z * log(z + 1 / (12 * z - 1 / z / 10)) - z + log(2 * PI / z) / 2;
	}
}

