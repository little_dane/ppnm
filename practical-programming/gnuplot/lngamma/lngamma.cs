using static System.Console;
using static System.Math;

class lngamma{
	static void Main(){
		double eps = 1.0 / 32, dx = 1.0 / 16;
		for(double x = -4 + eps; x <= 6; x += dx){
			WriteLine("{0,10:f3} {1,15:f8}", x, lngam(x));
		}
	}

	private static double lngam(double x){
		return x * Log(x + 1 / (12 * x - 1 / x / 10)) - x + Log(2 * PI / x) / 2;
	}
}

