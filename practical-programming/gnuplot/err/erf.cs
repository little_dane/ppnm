using static System.Console;
using static System.Math;

class errorfunction{
	static void Main(){
		double eps = 1.0 / 32, dx = 1.0 / 16;
		for(double x = -3 + eps; x <= 3; x += dx){
			WriteLine("{0,10:f3} {1,15:f8}", x, erf(x));
		}
	}

	private static double erf(double x){
		if(x <0){return -erf(-x);}
		double[] a = {0.254829592, -0.284496736, 1.421413741, -1.453152027, 1.061405429};
		double t = 1 / (1 + 0.3275911 * x);
		double sum = t * (a[0] + t* (a[1] + t * (a[2] + t * (a[3] + t * a[4]))));
		return 1 - sum * Exp(-x * x);
	}
}

