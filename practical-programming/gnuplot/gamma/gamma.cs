using static System.Console;
using static System.Math;

class gamma{
	static void Main(){
		double eps = 1.0 / 32, dx = 1.0 / 16;
		for(double x = -4 + eps; x <= 6; x += dx){
			WriteLine("{0,10:f3} {1,15:f8}", x, gam(x));
		}
	}

	private static double gam(double x){
		if(x < 0){
			return PI / Sin(PI * x) / gam(1 - x);
		}
		if(x < 20){
			return gam(x + 1) / x;
		}
		double lngamma = x * Log(x + 1 / (12 * x - 1 / x / 10)) - x + Log(2 * PI / x) / 2;
		return Exp(lngamma);
		
	}
}

